
var express = require('express');
var exec = require('child_process').exec;
var app = express();

var environment = process.env;
//environment.customProp = 'foo';

var params = {
    maxBuffer: 20000 * 1024,
    env: environment
};

var config = require("./config")

app.get('/', function(req, res){

	exec( config.executable + " " + req.query.url + ' -j', params , function(error, stdout, stderr){
		if (error != null){
			console.log(error);
			res.writeHead(404) ;
			res.end('404 - Not Found') ;
		}else {
			res.writeHead(200, {'Content-Type': 'application/json'});
			res.end(stdout);
			console.log('json sent to the client');
		}

	});
});

app.listen(config.port, config.address);

console.log('Server running at http://127.0.0.1:8080/');
